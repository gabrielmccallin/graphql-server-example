const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
// const AWS = require('aws-sdk');

// AWS.config.update({region: 'eu-west-2'});
// get current identity from the cli

// // Create an SQS service object
// var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

// var params = {};

// sqs.listQueues(params, function(err, data) {
//   if (err) {
//     console.log("Error", err);
//   } else {
//     console.log("Success", data.QueueUrls);
//   }
// });

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ typeDefs, resolvers });

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});