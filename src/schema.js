const { gql } = require('apollo-server');

const typeDefs = gql`
  # The "Query" type is the root of all GraphQL queries.
  type Query {
    holder(id: Int!): Holder
    account(id: Int!): Account
  }

  type Mutation {
    updateAccount(id: Int!, newBalance: Float): Account
  }

  type Holder {
    name: String!
    id: Int!
    accounts: [Account]
    nationality: String
  }

  type Account {
    id: Int!
    balance: Float!
    currency: String!
  }
`;

module.exports = typeDefs;