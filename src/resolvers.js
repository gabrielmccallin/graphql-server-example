const { holders, accounts } = require('./data');

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
    Query: {
        account: (parent, args) => accounts.find(account => account.id === args.id),
        holder: (parent, args) => holders.find(holder => holder.id === args.id)
    },
    Holder: {
        accounts: (parent, args) => parent.accounts.map(accountId => accounts.find(account => account.id === accountId))
    },
    Mutation: {
        updateAccount: (root, args) => {
            const { id, newBalance } = args;
            const account = accounts.find(account => account.id === id);
            account.balance = newBalance;
            return account;
        }
    },
};

module.exports = resolvers;