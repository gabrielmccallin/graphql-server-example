const holders = [{
  name: 'Usman',
  id: 1,
  accounts: [1, 2],
  nationality: 'India'
}, {
  name: 'Andy',
  id: 2,
  accounts: [3, 4, 5],
  nationality: 'Lithuania'
}, {
  name: 'Paul',
  id: 3,
  accounts: [6],
  nationality: 'New Zealand'
}, {
  name: 'Isha',
  id: 4,
  accounts: [7, 8, 9],
  nationality: 'France'
}, {
  name: 'Lukas',
  id: 5,
  accounts: [10, 11],
  nationality: 'Iran'
}, {
  name: 'Tuomas',
  id: 6,
  accounts: [12, 13],
  nationality: 'Ghana'
}, {
  name: 'Rob',
  id: 7,
  accounts: [14],
  nationality: 'China'
}, {
  name: 'Maryam',
  id: 8,
  accounts: [15, 16],
  nationality: 'Turkey'
}, {
  name: 'Jonas',
  id: 9,
  accounts: [17, 18, 19],
  nationality: 'Canada'
}, {
  name: 'Esra',
  id: 10,
  accounts: [20, 21],
  nationality: 'Germany'
}, {
  name: 'Greg',
  id: 11,
  accounts: [22],
  nationality: 'Finland'
}, {
  name: 'Gergo',
  id: 12,
  accounts: [23],
  nationality: 'South Africa'
}, {
  name: 'Mayuri',
  id: 13,
  accounts: [24, 25, 26],
  nationality: 'Hungary'
}, {
  name: 'Thato',
  id: 14,
  accounts: [27],
  nationality: 'Singapore'
}, {
  name: 'Aaron',
  id: 15,
  accounts: [28, 29],
  nationality: 'Spain'
}, {
  name: 'Jez',
  id: 16,
  accounts: [30],
  nationality: 'Vietnam'
}];

const currencies = ['GBP', 'USD', 'CNY', 'CAD', 'EUR', 'CHF', 'JPY'];

const round = (value, decimals) => {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
};

const genCurrency = () => {
  const index = Math.round(Math.random() * (currencies.length - 1));
  return currencies[index];
};

const genAccount = (index) => {
  return {
    id: index,
    currency: genCurrency(),
    balance: round((Math.random() * 1000000), 2)
  }
};

const genAccounts = () => {
  const empty = [...Array(29)];
  return empty.map((item, index) => genAccount(index + 1));
};

const accounts = genAccounts();

module.exports = { holders, accounts };